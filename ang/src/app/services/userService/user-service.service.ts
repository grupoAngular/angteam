import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  public userArr: any[] = [];
  public dataUserSource = new BehaviorSubject<any>('');

  constructor() {
    // console.log("servicio funciona");
  }

  //Acceso al arreglo de usuarios
   public getUser(dataUser: any) {
    //recoger datos
    // console.log(dataUser);
    
    //añadir a un array
    this.userArr.push(dataUser);
    // console.log(this.userArr);

    //Subscripción
    this.dataUserSource.subscribe(data => {
      console.log(data);
    });
    
    // emitir datos 
    this.dataUserSource.next(this.userArr);
    // this.dataUserSource.getValue(console.log);
    
    //desdel viewUserPage me debo subcribir a la dataUserSource = BehaviorSubject para recoger los datos que estoy emitiendo 
    //dentro del parentesis, en este caso el userArr
    

    //que debo hacer:
    //buscar la manera de subscribirme BehaviorSubject, que estoy emitiendo desde este servicio
    //luego mostrar la info
  }
}
