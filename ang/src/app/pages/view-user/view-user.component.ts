import { Component, OnInit } from '@angular/core';

import { UserService } from "../../services/userService/user-service.service";

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit {

  users: any;

  constructor(private userService: UserService ) { }

  ngOnInit(): void {
    this.users = this.userService.userArr;

    console.log(this.users);
  }

}
