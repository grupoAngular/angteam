import { Component, OnInit } from '@angular/core';

import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { UserService } from "../../services/userService/user-service.service";

@Component({
  selector: 'app-create-user',
  templateUrl: './create-user.component.html',
  styleUrls: ['./create-user.component.css']
})
export class CreateUserComponent implements OnInit {

  public formGroup: FormGroup;
  public user: any;

  constructor( private formBuilder: FormBuilder,
               private userService: UserService 
    ) { 
    
    this.createForm();
    // this.uploadDataForm();
  }

  ngOnInit(): void {
  }

  get nameNoValid() {
    return this.formGroup.get('name').invalid && this.formGroup.get('name').touched
  }
  get emailNoValid() {
    return this.formGroup.get('email').invalid && this.formGroup.get('email').touched
  }
  get passwordNoValid() {
    return this.formGroup.get('password').invalid && this.formGroup.get('password').touched
  }

  //2da posicion son las validaciones syncronos y luego los asyncronos. 
  createForm(){
    let minPassLength = 3;
    this.formGroup = this.formBuilder.group({
      name    : ['', [ Validators.required, Validators.minLength(minPassLength) ]],
      email   : ['', [ Validators.required, Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')] ],
      password: ['', [ Validators.required, Validators.minLength(minPassLength) ]],
    });
  }

  //Aqui se limpian los inputs
  // uploadDataForm(){
  //   this.formGroup.setValue({
  //   // this.formGroup.reset({
  //     // name: 'Daya',
  //     // email: 'daya@gmail.com',
  //     // password: '12345'
  //     name: '',
  //     email: '',
  //     password: ''
  //   });
  // }

  register(){
    console.log(this.formGroup);

    //valida al pulsar el boton register
    if ( this.formGroup.invalid) {
      return Object.values( this.formGroup.controls ).forEach( control => {
        control.markAsTouched();
      });
    }

    //servicio
    this.user = this.formGroup.value;
    this.userService.getUser(this.user);
    // console.log(this.user);
    
    this.formGroup.reset({
    });

  }
   
}
