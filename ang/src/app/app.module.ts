import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

// form Reactivo
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

//Routers
import { AppRoutingModule } from './app-routing.module';

//services
import { UserService } from "./services/userService/user-service.service";

//Components
import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './pages/home/home.component';
import { CreateUserComponent } from './pages/create-user/create-user.component';
import { ViewUserComponent } from './pages/view-user/view-user.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomeComponent,
    CreateUserComponent,
    ViewUserComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
